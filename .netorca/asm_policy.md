# ASM Policy Service

This will create a simple asm policy in BigIP Lab

> https://f5.lab.netorca.io:8443/

## Requesting

Please add the following configuration to your application file, under the 'services' heading

```yaml

services:
  asm_policy:
    - name: my_asm

```


### Validations

###TODO