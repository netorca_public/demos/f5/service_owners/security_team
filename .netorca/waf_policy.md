# Overview

This service will request a WAF policy that can be used on your VIP's

The WAF policy service is managed and approved by the security team and is based on an overall WAF standard for the organisation. 

## Use

waf_policy configuration will be as follows:

```yaml

waf_policy:
  - name: "policy1"
    serverTechnologies:
      - Java Servlets/JSP"
    disabled_signatures:
      - "200000001"
      
```

The fields are to be used as follows:
* Name -> this is a name for your WAF policy, it must be unique within your application
* serverTechnologies -> indicate the service technologies that should be allowed through your policy
* disabled_signatures -> optional: indicate the attack signatures you would like to disable for this policy. 