## security_team Service Owner demo repo

This repository is a Service Owner repository created for the security_team team.
The url of the created netorca instance is https://f5.demo.netorca.io/
The username and password you can use to log in as this team is:

username: security1
password: aut0m8t1on
